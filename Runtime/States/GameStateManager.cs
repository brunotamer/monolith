﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Monolith.States
{

    public sealed class GameStateManager
    {

        public readonly GameStateTreeStatic Tree;
        public event Action<GameState> StateLoaded;
        public event Action<GameState> StateEntered;
        public event Action<GameState> StateExited;
        public event Action<GameState> StateUnloaded;

        private readonly Stack<GameState> _stack;
        private GameStateStep _step;
        private bool _isReloadRequested;

        public GameStateManager(GameStateTreeStatic tree)
        {
            Tree = tree;

            _stack = new Stack<GameState>(1);
            TargetStateType = Tree.Default.Type;
            _step = GameStateStep.Uninitialized;
            _isReloadRequested = false;
        }

        public GameState CurrentState => _stack.Count == 0 ? null : _stack.Peek();
        public Type PreviousStateType { get; private set; }
        public Type TargetStateType { get; private set; }

        public T Find<T>() where T : GameState
        {
            T result = null;

            foreach (GameState state in _stack)
            {
                if (state is T castedState)
                {
                    result = castedState;
                    break;
                }
            }

            return result;
        }

        public void SetTarget<T>() where T : GameState
        {
            if (_step != GameStateStep.Active) throw new InvalidOperationException("Target cannot be set if state hasn't been entered.");

            Type type = CurrentState?.GetType();

            if (TargetStateType != type) throw new InvalidDataException("Target cannot be set if previously set target hasn't been reached.");

            Type target = typeof(T);

            if (!Tree.Contains(target)) throw new ArgumentException("Target state could not be found.");

            PreviousStateType = type;
            TargetStateType = target;
        }

        public void ReloadCurrent()
        {
            if (_step != GameStateStep.Active) throw new InvalidOperationException("Target cannot be set if state hasn't been entered.");

            Type type = CurrentState?.GetType();

            if (TargetStateType != type) throw new InvalidDataException("Target cannot be set if previously set target hasn't been reached.");

            _isReloadRequested = true;
        }

        internal void BeginFrame(Game game)
        {
            bool isDone = false;

            do
            {
                GameState currentState = CurrentState;

                Type current = currentState?.GetType();
                Type parent = Tree.FindParent(current)?.Type;
                GameStateTreeNode nextNode = Tree.FindNext(current, TargetStateType);
                Type next = nextNode?.Type;

                switch (_step)
                {
                    case GameStateStep.Uninitialized:
                        currentState = nextNode.Make(game);

                        _stack.Push(currentState);

                        _step = GameStateStep.Loading;

                        break;
                    case GameStateStep.Loading:
                        if (currentState.Load(game))
                        {
                            _step = GameStateStep.Loaded;
                            StateLoaded?.Invoke(currentState);
                        }
                        else
                        {
                            isDone = true;
                        }

                        break;
                    case GameStateStep.Loaded:
                        if (_isReloadRequested)
                        {
                            isDone = true;
                            break;
                        }

                        if (current == next)
                        {
                            _step = GameStateStep.Activating;
                        }
                        else if (next != parent)
                        {
                            _step = GameStateStep.Uninitialized;
                        }
                        else
                        {
                            isDone = true;
                        }

                        break;
                    case GameStateStep.Activating:
                        currentState.Enter(game);
                        _step = GameStateStep.Active;

                        StateEntered?.Invoke(currentState);

                        isDone = true;

                        break;
                    default:
                        isDone = true;
                        break;
                }
            } while (!isDone);
        }

        internal void Update(Game game)
        {
            GameState currentState = _stack.Peek();

            Type current = currentState.GetType();
            Type next = Tree.FindNext(current, TargetStateType)?.Type;

            bool isTarget = current == next;

            if (isTarget && (_step == GameStateStep.Active))
            {
                currentState.Update(game);
            }
        }

        internal void EndFrame(Game game)
        {
            bool isDone;

            do
            {
                GameState currentState = CurrentState;

                Type current = currentState?.GetType();
                Type parent = Tree.FindParent(current)?.Type;
                Type next = Tree.FindNext(current, TargetStateType)?.Type;

                switch (_step)
                {
                    default:
                        isDone = true;
                        break;
                    case GameStateStep.Active:
                        isDone = !_isReloadRequested && current == next;

                        if (!isDone) _step = GameStateStep.Deactivating;

                        break;
                    case GameStateStep.Deactivating:
                        currentState.Exit(game);
                        isDone = false;
                        _step = GameStateStep.Loaded;

                        StateExited?.Invoke(currentState);

                        break;
                    case GameStateStep.Loaded:
                        isDone = !_isReloadRequested && next != parent;

                        if (!isDone) _step = GameStateStep.Unloading;

                        break;
                    case GameStateStep.Unloading:
                        isDone = !currentState.Unload(game);

                        if (!isDone)
                        {
                            _stack.Pop();

                            if (_stack.Count == 0)
                            {
                                _step = GameStateStep.Uninitialized;
                            }
                            else
                            {
                                _step = GameStateStep.Loaded;
                            }

                            _isReloadRequested = false;

                            StateUnloaded?.Invoke(currentState);
                        }
                        break;
                }
            } while (!isDone);
        }

    }

}
